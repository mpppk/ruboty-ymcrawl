require_relative '../ymcrawl/main'
require 'singleton'

module Ruboty
  module Handlers

    class CrawlManager
      include Singleton
      def initialize
        @crawl = nil
      end

      def get_crawl
        @crawl = Ruboty::YMCrawl::Core.new if @crawl == nil
        @crawl
      end
    end

    class YMCrawl < Base
      on(/hello\z/i, name: "hello", description: "Return hello")

      def hello(message)
        message.reply("hello!!")
      end
    end

    class Crawl < Base
      on(
        /crawl ?(?<url>.+)?\z/i,
        name: "crawl",
        description: "crawl image"
      )

      def get_access_token_message(url)
        return "You don't have access token.
        1. Go to: #{url}
        2. Click \"Allow\" (you might have to log in first).
        3. reply to bot as \"@bot dropbox:auth (auth_code) \""
      end

      def crawl(message)
        puts "crawl start in handlers"
        url = (message[:url] == nil) ? "-- please set url --" : message[:url]
        begin
          crawl = CrawlManager.instance.get_crawl
          uploader = crawl.get_uploader

          # upload先がlocal以外かつアクセストークンが取得されていない場合は、取得先URLを示して終了
          if not uploader.access_token? and uploader.get_name != "local"
            message.reply( get_access_token_message( uploader.get_access_token_url ) )
            return nil
          end

          message.reply("rubot is crawling from #{url}")
          zip_paths = crawl.start([url])
          message.reply("get zip file => #{zip_paths}")
        rescue URI::InvalidURIError => ex
          puts ex
          message.reply("URL is invalid. please retry.")
        rescue => ex
          puts "error raise in Crawl.crawl"
          puts ex
          message.reply("Sorry, error occurred.")
          message.reply("Please feedback this error to niboshiporipori@gmail.com")
          message.reply(ex)
        end
      end
    end

    class VerifyAuthCode < Base
      on(
        /dropbox:auth ?(?<auth_code>.+)?\z/i,
        name: "verify_auth_code",
        description: "add access token by auth code"
      )

      def verify_auth_code(message)
        auth_code = (message[:auth_code] == nil) ? "-- please set auth_code --" : message[:auth_code]
        uploader = CrawlManager.instance.get_crawl.get_uploader
        access_token = uploader.verify_auth_code(auth_code)
        Ruboty::YMCrawl::DataManager.instance.update_access_token(uploader.get_name, access_token)

        message.reply("You added access token!")
        message.reply("Try clawling again!")
      end
    end

  end
end
