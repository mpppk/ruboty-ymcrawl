# Install this the SDK with "gem install dropbox-sdk"
require 'dropbox_sdk'
module Ruboty
	module YMCrawl
		class DropboxManager

			def initialize(app_key, app_sec)
				@app_key = app_key
				@app_sec = app_sec
				@client = nil
				@access_token = nil
			end

			def login(arg_access_token = nil)
				if not @client.nil?
					puts "already logged in!"
					return @access_token
				end

				@access_token = arg_access_token
				begin
					@client = DropboxClient.new(@access_token)
					puts "account info: #{@client.account_info()}"
					return @access_token
				rescue DropboxError => ex
					puts "---- access token is invalid ----"
					return nil
				end
			end
			
			def get_auth_code_url
				puts "web_auth is nil!!!!" if @web_auth == nil
				puts "@app_key: #{@app_key}"
				puts "@app_sec: #{@app_sec}"
				@web_auth = DropboxOAuth2FlowNoRedirect.new(@app_key, @app_sec)
				authorize_url = @web_auth.start()
			end

			def get_access_token(auth_code)
	            @web_auth.finish(auth_code)[0]
			end

			def put(command)
				fname = command[0]

				#If the user didn't specifiy the file name, just use the name of the file on disk
				if command[1]
					new_name = command[1]
				else
					new_name = File.basename(fname)
				end

				if fname && !fname.empty? && File.exists?(fname) && (File.ftype(fname) == 'file') && File.stat(fname).readable?
					#This is where we call the the Dropbox Client
					pp @client.put_file(new_name, open(fname))
				else
					puts "couldn't find the file #{ fname }"
				end
			end

			def get_share_link(path) @client.shares(path) end
		end
end
end
