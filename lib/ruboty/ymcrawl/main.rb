require_relative 'crawler'
require_relative 'dropbox.rb'
require 'optparse'
require 'json'
require 'zipruby'
require 'find'
require 'kconv'
require 'json-schema'

module Ruboty
	module YMCrawl
		ORG_SETTING_FILE_PATH     = "YMCrawlfile"
		SETTING_FILE_PATH         = "#{ORG_SETTING_FILE_PATH}"
		SCHEMA_FILE_PATH          = "YMCrawl_schema.json"
		UPLOADER_SCHEMA_FILE_PATH = "uploader_schema.json"
		SITE_JSON_PATH            = "site.json"
	  
	  class DataManager

	    include Singleton

	    def initialize
	    	@setting = JSON.parse( File.open(SETTING_FILE_PATH).read)
	    	puts "setting: #{@setting}"
	    	puts "YMCrawlfile valid"
	    	puts JSON::Validator.fully_validate(SCHEMA_FILE_PATH, @setting, :insert_defaults => true).to_s
	    	@sites = get_sites_json(SITE_JSON_PATH)
	    	File.write( SITE_JSON_PATH, JSON.unparse(@sites) ) unless FileTest.exist?(SITE_JSON_PATH)
	    	puts "uploader valid"
	    	puts "uploder data: #{get_uploader_data}"   
	    	puts JSON::Validator.fully_validate(UPLOADER_SCHEMA_FILE_PATH, get_uploader_data, :insert_defaults => true).to_s
	    end

	    # 各サイトごとの、画像取得のためのcssセレクタを記載したjsonをファイルから取得して返す
	    def get_sites_json(path)
	    	path = FileTest.exist?(path) ? path : @setting["site_json"]
	    	puts "reading site json file from #{path}"
	    	JSON.parse( open(path).read)
	    end

	    # URLのドメインに合致するsite情報を返す
	    def get_current_uploder_info(url)
	    	host = URI(url).host
	    	# ハッシュのkeyがs[0],valueがs[1]に入る
	    	@sites.each{ |s| return s[1] if s[1]["host"] == host }
	    	return @sites["default"]
	    end

	    def update_access_token(uploader_name, access_token)
	    	@setting["uploader"][uploader_name]["access_token"] = access_token if @setting["uploader"][uploader_name] != access_token
	    	puts "setting: #{@setting}"
	    	open(SETTING_FILE_PATH, 'w') do |io|
	    		JSON.dump(@setting, io)
	    	end
	    end

	    def get_setting;                  @setting                                         end
	    def get_save_to;                  @setting["save_to"]                              end
	    def get_uploader_data;            @setting["uploader"][get_save_to]                end
	    def get_current_access_token;     get_uploader_data["access_token"]                end
	    def get_current_app_key;          ENV["#{@setting["save_to"].upcase }_APP_KEY"]    end
	    def get_current_app_secret;       ENV["#{@setting["save_to"].upcase }_APP_SECRET"] end
	  end

		class Core
			def initialize
				@data = DataManager.instance
				if @data.get_save_to != "local"
					@uploader = Uploader.new(@data.get_save_to, @data.get_current_app_key, @data.get_current_app_secret, @data.get_current_access_token)
					puts "env dropbox app key: #{@data.get_current_app_key}"
					puts "env dropbox app sec: #{@data.get_current_app_secret}"
				end
			end

			def start(urls); upload crawl(urls) end

			# 画像をクロールして保存する。保存したファイルのパスを返す。
			def crawl(urls)
				puts "in crawl"
				ncrawler  = Crawler.new(@data.get_setting["dst_dir"], @data.get_current_uploder_info(urls[0]), @data.get_setting["wait_time"])
				urls.map{ |v| ncrawler.save_images(v) }
			end

			# 画像を指定した先へアップロード
			def upload(file_dirs)
				puts "in upload"
				setting  = @data.get_setting
				return nil if @data.get_save_to == "local"

				@uploader.login(@data.get_current_access_token)
				zip_paths = file_dirs.map{ |dir| zip_dir(dir) }
				encode = (ENV["LANG"] == nil) ? "utf-8" : ENV["LANG"]
				begin
					file_dirs.each{ |dir| FileUtils::remove_entry_secure( dir.force_encoding(encode) ) }
				rescue
					if encode != "ascii-8bit"
						encode = "ascii-8bit"
						retry
					end
				end
				share_paths = []
				zip_paths.each do |path|
					puts "uploading #{path} to dropbox"
					put_result = @uploader.put([path])
					File::delete(path)
					share_paths << @uploader.get_share_link(put_result["path"])["url"]
				end
				return share_paths
			end

			# 指定されたディレクトリ以下のファイルをzipにする。返り値はzipのパス
			def zip_dir(src)
				dst = "#{src}.zip"
				Zip::Archive.open(dst, Zip::CREATE) do |ar|
					Dir.glob("#{src}/*").each do |item|
						ar.add_file(item)
					end
				end
				dst
			end

			def get_uploader; @uploader end
		end

		# ファイルをアップロードする先を抽象化したクラス
		class Uploader
			def initialize(name, app_key, app_secret, access_token = nil)
				@name         = name
				@app_key      = app_key
				@app_secret   = app_secret
				@access_token = access_token
				@c_uploader   = create_uploader
			end

			# 引数に応じてアップロード先のインスタンスを返す
			def create_uploader
				return @c_uploader unless @c_uploader == nil
				if @name == "dropbox"
					@c_uploader = DropboxManager.new(@app_key, @app_secret) 
					return @c_uploader
				end
				raise ArgumentError("uploader #{@name} is not found")
			end

			def access_token?; @access_token != "" and @access_token != nil end

			def verify_auth_code(auth_code)
				@access_token = @c_uploader.get_access_token(auth_code)
			end

			def login(token = nil)
				@access_token = (token == nil) ? @access_token : token
				puts "access token: #{@access_token}"
				puts "---- access token isn't set when login!!!! ----" if token ==nil
			  @c_uploader.login(token)
			end

			def get_access_token_url
				error = "---- YMCrawl publishing new access token url. But you already have access token. ----" 
				puts error if @access_token != nil and @access_token != ""
				@c_uploader.get_auth_code_url
			end

			def get_name;    @name                    end
			def put(command) @c_uploader.put(command) end
			def get_share_link(path) @c_uploader.get_share_link(path) end
		end
	end
end