# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruboty/ymcrawl/version'

Gem::Specification.new do |spec|
  spec.name          = "ruboty-ymcrawl"
  spec.version       = Ruboty::Ymcrawl::VERSION
  spec.authors       = ["mpk"]
  spec.email         = ["niboshiporipori@gmail.com"]
  spec.summary       = %q{image crawler for ruboty}
  spec.description   = %q{hogehoge}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # spec.add_dependency "bundler"
  spec.add_dependency 'addressable'
  spec.add_dependency 'json'
  spec.add_dependency 'dropbox-sdk'
  spec.add_dependency 'zipruby'
  spec.add_dependency 'json-schema'
  spec.add_dependency 'nokogiri'

  spec.add_runtime_dependency "bundler", "~> 1.7", ">= 0"

  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
end
